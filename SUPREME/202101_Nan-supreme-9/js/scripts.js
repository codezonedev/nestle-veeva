$(document).ready(function(){
	$('.reference-button').on('click', function(){
		var content_ = $(this).closest('.reference-holder').find('.reference-txt');
		if(content_.is(':hidden')){
            content_.fadeIn();   
            $('.page-holder').append('<div class="overlay"></div>');
        } else {
            content_.fadeOut();
            $('.overlay').remove();
        }
	});

    $(document).on('click', '.overlay', function(e){
        e.preventDefault();
        $('.reference-button').trigger('click');
    });


	// Utilizar quando tiver que chamar outra tela
	$('.goSlide').on('click', function(){
		var slide_ = $(this).attr('href');
		JSGoToSlide(slide_);
    });
    

    $('.open-popup, .close-popup').on('click', function(){
        $('body').toggleClass('modal-active');
        $('.modal').fadeToggle();
    });
});

// Função para passar para o próximo slide
$(window).on('swipeleft', function(e) {
    if (slideNumber == numberSlides) return false;
    JSGoToSlide(baseName+'-'+(++slideNumber)+'.zip');
});

// Função para passar para o slide anterior
$(window).on('swiperight', function(e) {
    if (slideNumber == 1) return false;
    JSGoToSlide(baseName+'-'+(--slideNumber)+'.zip');
});